from flask import Flask, request, render_template, redirect, url_for
from flask_pymongo import PyMongo
from bson.objectid import ObjectId

app = Flask(__name__)

#app.config['MONGO_URI'] = 'mongodb://localhost:27017/challenge'
app.config['MONGO_URI'] = 'mongodb+srv://costin:root@cluster0.kcpza.mongodb.net/challenge?retryWrites=true&w=majority'

mongo = PyMongo(app)


@app.route('/', methods=['GET', 'POST'])
def index():
    """
    Fonction GET, POST :
    Cette fonction nous permet d'aller chercher toutes les informations dans la base de données.
    Elle va également nous servir pour la barre de recherche qui permet de filtrer les informations de la base de données.
    Pour finir, elle va aussi renvoyer un fichier index.html qui nous permettra d'afficher les informations de la base de données.
    :return:
    """
    if request.method == "POST":
        recherche = request.form.get('recherche')
        filtre = request.form.get('filtre')
        if recherche:
            if filtre == 'tag':
                images = mongo.db.challenge.find({"tag": {"$regex": recherche}})
                return render_template('index.html', images=images)
            elif filtre == 'title':
                images = mongo.db.challenge.find({"title": {"$regex": recherche}})
                return render_template('index.html', images=images)
    else:
        images = mongo.db.challenge.find()
        return render_template('index.html', images=images)


@app.route('/add-image')
def add_image():
    """
    Fonction GET :
    Cette fonction appelle le fichier add_image.
    C'est ce fichier qui va nous permettre d'ajouter une image ainsiq que ses caractéristiques
    :return:
    """
    return render_template('add_image.html')


@app.route('/upload-image', methods=['POST'])
def create_image():
    """
    Fonction POST :
    Cette fonction est utilisé pour upload l'image ainsi que ses caractéristiques
    :return:
    """
    if 'files[]' in request.files and 'is_human' in request.form and 'tag[]' in request.form and 'title' in request.form and 'type_image' in request.form and 'is_product' in request.form and 'photo_is_institu' in request.form and 'format' in request.form and 'credit' in request.form and 'copyright' in request.form and 'limit_utilisation' in request.form and 'date_end_right' in request.form:
        files = request.files.getlist('files[]')
        tag = request.form.getlist('tag[]')
        title = request.form.get('title')
        type_image = request.form.get('type_image')
        is_product = request.form.get('is_product')
        photo_is_institu = request.form.get('photo_is_institu')
        format = request.form.get('format')
        credit = request.form.get('credit')
        limit_utilisation = request.form.get('limit_utilisation')
        copyright = request.form.get('copyright')
        date_end_right = request.form.get('date_end_right')
        is_human = request.form.get('is_human')

        if type_image == 'passion_froid':
            type_image = 'passion_froid'
        elif type_image == 'fournisseur':
            type_image = 'fournisseur'
        else:
            type_image = 'logo'

        if is_product == 'true':
            is_product = True
        else:
            is_product = False

        if is_human == 'true':
            is_human = True
        else:
            is_human = False

        if photo_is_institu == 'true':
            photo_is_institu = True
        else:
            photo_is_institu = False

        if format == 'vertical':
            format = 'vertical'
        else:
            format = 'horizontal'

        if limit_utilisation == 'true':
            limit_utilisation = True
        else:
            limit_utilisation = False

        for file in files:
            mongo.save_file(file.filename, file)
            mongo.db.challenge.insert({'files_name': file.filename, 'is_human': is_human, 'tag': tag, 'title': title, 'type_image': type_image, 'is_product': is_product, 'photo_is_institu': photo_is_institu, 'format': format, 'credit': credit, 'limit_utilisation': limit_utilisation, 'copyright': copyright, 'date_end_right': date_end_right})
    return redirect(url_for('index'))


@app.route('/file/<filename>')
def file(filename):
    """
    Fonction GET :
    Cette fonction nous permet d'envoyer des images présentes précises de la base de données à la vue.
    :param filename:
    :return:
    """
    return mongo.send_file(filename)


@app.route('/image/<id>')
def detail_image(id):
    """
    Fonction GET :
    Cette fonction va nous renvoyer le fichier details_image.html.
    Cela va nous permettre d'afficher toutes les informations d'une image.
    :param id:
    :return:
    """
    filename = mongo.db.challenge.find_one({'_id': ObjectId(id)})
    return render_template('details_image.html', filename=filename)


@app.route('/delete-image/<id>', methods=['POST'])
def delete_image(id):
    """
    Fonction POST :
    Cette fonction nous permet de supprimer une photo et tous ses détails.
    :param id:
    :return:
    """
    mongo.db.challenge.delete_one({'_id': ObjectId(id)})
    return redirect(url_for('index'))


@app.route('/update-image/<id>', methods=['POST'])
def update_image(id):
    """
    Fonction POST :
    Cette fonction envoie les nouvelles informations mise à jour par l'utilisateur.
    :param id:
    :return:
    """
    files = request.files.getlist('files[]')
    tag = request.form.getlist('tag[]')
    title = request.form.get('title')
    type_image = request.form.get('type_image')
    is_product = request.form.get('is_product')
    photo_is_institu = request.form.get('photo_is_institu')
    format = request.form.get('format')
    credit = request.form.get('credit')
    limit_utilisation = request.form.get('limit_utilisation')
    copyright = request.form.get('copyright')
    date_end_right = request.form.get('date_end_right')
    is_human = request.form.get('is_human')

    updated_image = mongo.db.challenge.find_one({'_id': ObjectId(id)})
    updated_image['files'] = files
    updated_image['tag'] = tag
    updated_image['title'] = title
    updated_image['type_image'] = type_image
    updated_image['is_product'] = is_product
    updated_image['photo_is_institu'] = photo_is_institu
    updated_image['format'] = format
    updated_image['credit'] = credit
    updated_image['limit_utilisation'] = limit_utilisation
    updated_image['copyright'] = copyright
    updated_image['date_end_right'] = date_end_right
    updated_image['is_human'] = is_human

    mongo.db.challenge.replace_one({'_id': ObjectId(id)}, updated_image)
    return redirect(url_for('detail_image', id=id))


@app.route('/test', methods=['GET'])
def tes():
    return render_template('test.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)